import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../core/interfaces/Product';
import {ShopService} from '../../core/services/shop.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {

  @Input() product: Product;

  constructor(
    private shopService: ShopService
  ) { }

  ngOnInit(): void {

  }

  addToCart(product) {
    const currentValue = this.shopService.selectedProductsSource.value;
    const updatedValue = [...currentValue, product];

    this.shopService.selectedProductsSource.next(updatedValue);
  }

}
