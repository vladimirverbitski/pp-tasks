import {Component} from '@angular/core';
import {ApiService} from '../../core/services/api.service';

@Component({
  selector: 'app-shop-window',
  templateUrl: './shop-window.component.html',
  styleUrls: ['./shop-window.component.css']
})
export class ShopWindowComponent {

  public product$ = this.api.getProductList();

  constructor(
    private api: ApiService
  ) { }

}
