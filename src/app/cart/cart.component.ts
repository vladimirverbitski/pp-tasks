import {Component, OnDestroy, OnInit} from '@angular/core';
import {CurrencyService} from '../core/services/currency.service';
import {ShopService} from '../core/services/shop.service';
import {takeUntil} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';
import {ExchangeRateResponse} from '../core/interfaces/exchange-rate-response';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {

  private subscription$: ReplaySubject<any> = new ReplaySubject<any>();

  public cartProducts;

  public totalPriceDollars;

  public currencyConvertTo = ['RUB', 'EUR', 'GBP', 'JPY', 'USD'];

  public selectedCurrency = '';

  public totalPrice;


  constructor(
    private currencyService: CurrencyService,
    public shopService: ShopService
  ) { }

  ngOnInit(): void {
    this.currencyService.getExchangeRate()
      .pipe(
        takeUntil(this.subscription$)
      )
      .subscribe((it: ExchangeRateResponse) => {
        this.currencyService.proceedRates(it.rates);
    });

    this.shopService.selectedProducts$
      .pipe(
        takeUntil(this.subscription$)
      )
      .subscribe( it => {

        this.cartProducts = it;
        this.totalPriceDollars = it
          .map(e => +e.price)
          .reduce((sum, current) => sum + current, 0)
          .toFixed(2);
    });
  }

  getConvertedPrice(currency) {
    return this.currencyService.convertPrice(this.totalPriceDollars, currency);
  }

  clearCart() {
    this.shopService.selectedProductsSource.next([]);
  }

  onSelectCurrencyType(currency, value) {

    this.selectedCurrency = currency;
    this.totalPrice = value;
  }

  buyProducts() {
    this.selectedCurrency = '';
    this.totalPrice = '';
    this.shopService.selectedProductsSource.next([]);
  }

  ngOnDestroy(): void {
    this.subscription$.next();
    this.subscription$.complete();
  }

}
