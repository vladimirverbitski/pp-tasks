import { Component, OnInit } from '@angular/core';
import {ShopService} from '../services/shop.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(
    public shopService: ShopService
  ) { }

  ngOnInit(): void {
  }

}
